/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package model.extras.exceptions;

public abstract class AbstractIPMonitorException extends Exception {

    public AbstractIPMonitorException() {
    }

    public AbstractIPMonitorException(String message) {
        super(message);
    }
}
