/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package model.extras.exceptions;

import model.*;

public class InvalidIPAddressException extends AbstractIPMonitorException {
}
