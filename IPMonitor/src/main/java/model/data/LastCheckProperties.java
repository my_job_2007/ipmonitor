/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package model.data;

import java.util.*;

public class LastCheckProperties {

    public static final String LAST_IP = "LastIP";
    public static final String LAST_IP_VALUE = "";
    public static final String LAST_CHECKED = "LastChecked";
    public static final Date LAST_CHECKED_VALUE = null;
    public static final String LAST_CHANGE = "LastChange";
    public static final Date LAST_CHANGE_VALUE = null;
}
