/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package model.ipmonitor.exceptions;

import model.extras.exceptions.AbstractIPMonitorException;

public class InvalidIntervalException extends AbstractIPMonitorException {

}
