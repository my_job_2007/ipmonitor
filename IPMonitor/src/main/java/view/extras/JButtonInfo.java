/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package view.extras;

import javax.swing.JButton;

public class JButtonInfo extends JButton {

    public JButtonInfo() {
        setText("Info");
    }
}
