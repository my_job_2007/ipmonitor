/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package view.extras;

import java.awt.*;
import javax.swing.*;

public class JLabelRequiredString extends JLabel {

    public JLabelRequiredString() {
        setText("Required field");
    }
}
