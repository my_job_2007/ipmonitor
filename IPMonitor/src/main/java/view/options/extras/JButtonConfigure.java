/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package view.options.extras;

import javax.swing.*;

public class JButtonConfigure extends JButton {

    public JButtonConfigure() {
        setText("Configure...");
    }
}
