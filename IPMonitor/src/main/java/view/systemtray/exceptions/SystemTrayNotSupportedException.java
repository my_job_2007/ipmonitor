/*
 * Copyright (C) 2007 - 2010 Gabriel Zanetti
 */
package view.systemtray.exceptions;

import model.extras.exceptions.AbstractIPMonitorException;

public class SystemTrayNotSupportedException extends AbstractIPMonitorException {
}


